﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;
using W8.Interfaces;

namespace W8.Model
{
    /*
     * класс книги, представляющей художественную литературу
     */
    public class FictionBook : Book, IMyCloneable<FictionBook>, ICloneable
    {
        public bool IsFiction { get; set; }

        public FictionBook(string _name, string _author, int _pages, bool _isFiction) : base(_name, _author, _pages)
        {
            IsFiction = _isFiction;
        }

        public new FictionBook Copy()
        {
            return new(Name, Author, PageNumber, IsFiction);
        }

        public new string ToString()
        {
            return String.Concat(new string[] { Name, Author, PageNumber.ToString(), IsFiction.ToString()});
        }

        public object Clone()
        {
            return Copy();
        }
    }
}
