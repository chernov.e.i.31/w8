﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;
using W8.Interfaces;

namespace W8.Model
{
    /*
     * разновидность художественной книги, фентези
     */
    public class FantasyFictionBook : FictionBook, IMyCloneable<FantasyFictionBook>, ICloneable
    {
        public bool IsFantasy { get; set; }

        public FantasyFictionBook(string _name, string _author, int _pages, bool _isFiction, bool _isFantasy) : base(_name, _author, _pages, _isFiction)
        {
            IsFantasy = IsFantasy;
        }

        public new FantasyFictionBook Copy()
        {
            return new(Name, Author, PageNumber, IsFiction, IsFantasy);
        }

        public new string ToString()
        {
            return String.Concat(new string[] { Name, Author, PageNumber.ToString(), IsFiction.ToString(), IsFantasy.ToString() });
        }

        public object Clone()
        {
            return Copy();
        }
    }
}
