﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;
using W8.Interfaces;

namespace W8.Model
{
    /*
     * разновидность художественной книги, фантастика
     */
    public class ScienceFictionBook : FictionBook, IMyCloneable<ScienceFictionBook>, ICloneable
    {
        public bool IsScience { get; set; }

        public ScienceFictionBook(string _name, string _author, int _pages, bool _isFiction, bool _isScience) : base(_name, _author, _pages, _isFiction)
        {
            IsScience = _isScience;
        }

        public new ScienceFictionBook Copy()
        {
            return new(Name, Author, PageNumber, IsFiction, IsScience);
        }

        public new string ToString()
        {
            return String.Concat(new string[] { Name, Author, PageNumber.ToString(), IsFiction.ToString(), IsScience.ToString() });
        }

        public object Clone()
        {
            return Copy();
        }
    }
}