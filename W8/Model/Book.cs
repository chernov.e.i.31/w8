﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using W8.Interfaces;

namespace W8.Model
{
    /*
     * Главный класс книга
     */
    public class Book : IMyCloneable<Book>, ICloneable
    {
        public string Name { get; set; }
        public int PageNumber { get; set; }
        public string Author { get; set; }

        public Book(string _name, string _author, int _pages)
        {
            Name = _name;
            Author = _author;
            PageNumber = _pages;
        }

        public Book Copy()
        {
            return new(Name, Author, PageNumber);
        }

        public new string ToString()
        {
            return String.Concat(new string[] { Name, Author, PageNumber.ToString() });
        }

        public object Clone()
        {
            return Copy();
        }
    }
}
