﻿using W8.Model;

var book = new Book("Книга", "Автор", 10);
var book1 = book.Copy();
book.Name = "Книга0";
Print(book.ToString());
Print(book1.ToString());

var bookFiction = new FictionBook("КнигаХудожественная", "Автор1", 10, true);
var bookFiction1 = bookFiction.Copy();
bookFiction.Author = "Автор2";
Print(bookFiction.ToString());
Print(bookFiction1.ToString());

var bookFantasy = new FantasyFictionBook("Фантастика", "Автор", 20, true, true);
var bookFantasy1 = bookFantasy.Copy();
bookFantasy.PageNumber = 50;
Print(bookFantasy.ToString());
Print(bookFantasy1.ToString());

var bookScience = new ScienceFictionBook("Книга", "Автор", 40, true, true);
var bookScience1 = bookScience.Copy();
bookScience.IsScience = false;
Print(bookScience.ToString());
Print(bookScience1.ToString());

void Print(string message)
{
    Console.WriteLine(message);
}