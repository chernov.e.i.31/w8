using W8.Model;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestCopyBook()
        {
            var book = new Book("�����", "�����", 10);
            var book1 = book.Copy();

            Assert.AreNotEqual(book1, book);
            Assert.AreEqual(book1.Author, book.Author);
            Assert.AreEqual(book1.Name, book.Name);
            Assert.AreEqual(book1.PageNumber, book.PageNumber);
        }

        [TestMethod]
        public void TestCopyFictionBook()
        {
            var book = new FictionBook("��������������", "�����", 10, true);
            var book1 = book.Copy();

            Assert.AreNotEqual(book1, book);
            Assert.AreEqual(book1.Author, book.Author);
            Assert.AreEqual(book1.Name, book.Name);
            Assert.AreEqual(book1.PageNumber, book.PageNumber);
            Assert.AreEqual(book1.IsFiction, book.IsFiction);
        }

        [TestMethod]
        public void TestCopyScienceFictionBook()
        {
            var book = new ScienceFictionBook("����������", "�����", 10, true, true);
            var book1 = book.Copy();

            Assert.AreNotEqual(book1, book);
            Assert.AreEqual(book1.Author, book.Author);
            Assert.AreEqual(book1.Name, book.Name);
            Assert.AreEqual(book1.PageNumber, book.PageNumber);
            Assert.AreEqual(book1.IsFiction, book.IsFiction);
            Assert.AreEqual(book1.IsScience, book.IsScience);
        }

        [TestMethod]
        public void TestCopyFantasyFictionBook()
        {
            var book = new FantasyFictionBook("�������", "�����", 10, true, false);
            var book1 = book.Copy();

            Assert.AreNotEqual(book1, book);
            Assert.AreEqual(book1.Author, book.Author);
            Assert.AreEqual(book1.Name, book.Name);
            Assert.AreEqual(book1.PageNumber, book.PageNumber);
            Assert.AreEqual(book1.IsFiction, book.IsFiction);
            Assert.AreEqual(book1.IsFantasy, book.IsFantasy);
        }

        [TestMethod]
        public void TestCloneBook()
        {
            var book = new Book("�����", "�����", 10);
            var book1 = (Book)book.Clone();

            Assert.AreNotEqual(book1, book);
            Assert.AreEqual(book1.Author, book.Author);
            Assert.AreEqual(book1.Name, book.Name);
            Assert.AreEqual(book1.PageNumber, book.PageNumber);
        }

        [TestMethod]
        public void TestCloneFictionBook()
        {
            var book = new FictionBook("��������������", "�����", 10, true);
            var book1 = (FictionBook)book.Clone();

            Assert.AreNotEqual(book1, book);
            Assert.AreEqual(book1.Author, book.Author);
            Assert.AreEqual(book1.Name, book.Name);
            Assert.AreEqual(book1.PageNumber, book.PageNumber);
            Assert.AreEqual(book1.IsFiction, book.IsFiction);
        }

        [TestMethod]
        public void TestCloneScienceFictionBook()
        {
            var book = new ScienceFictionBook("����������", "�����", 10, true, true);
            var book1 = (ScienceFictionBook)book.Clone();

            Assert.AreNotEqual(book1, book);
            Assert.AreEqual(book1.Author, book.Author);
            Assert.AreEqual(book1.Name, book.Name);
            Assert.AreEqual(book1.PageNumber, book.PageNumber);
            Assert.AreEqual(book1.IsFiction, book.IsFiction);
            Assert.AreEqual(book1.IsScience, book.IsScience);
        }

        [TestMethod]
        public void TestCloneFantasyFictionBook()
        {
            var book = new FantasyFictionBook("�������", "�����", 10, true, false);
            var book1 = (FantasyFictionBook)book.Clone();

            Assert.AreNotEqual(book1, book);
            Assert.AreEqual(book1.Author, book.Author);
            Assert.AreEqual(book1.Name, book.Name);
            Assert.AreEqual(book1.PageNumber, book.PageNumber);
            Assert.AreEqual(book1.IsFiction, book.IsFiction);
            Assert.AreEqual(book1.IsFantasy, book.IsFantasy);
        }
    }
}